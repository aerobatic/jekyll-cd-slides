# Jekyll Continuous Deployment with Docker

This is a presentation I gave as a lightning talk at [JekyllConf 2016](http://jekyllconf.com/). Naturally the slide deck itself is a Jekyll site built with [reveal.js]() and continuously deployed to [Aerobatic](https://www.aerobatic.com).

[https://jekyll-cd-slides.aerobatic.io](https://jekyll-cd-slides.aerobatic.io)
